import React, {useState} from 'react'



const ContactOne = () => {
  const [data, setData ] = useState({
      firstName:'', 
      lastName:'', 
      phone:'', 
      email:''
  });


  const handlChange = (e) => {
      e.preventDefault();

      setData({...data, [e.target.name]: e.target.value});
      console.log(data);
   
  }

  const handleSubmit = () => {
      console.log(data);
  }
    return (
    <section>
    <div className="container scrollspy" id="contact">
      <div className="row">
        <div className="col m6 s12" data-aos="zoom-out-right">
         <h3 className="large" >Connecting Serbia with Compassionate Physicians</h3><br />
         <button className="btn waves-effect primary btn-large pulse primary-hover"   name="action" >Apllay only today
            <i className="material-icons right">send</i>
          </button> 
          <div className="row">
             <div className="col s12">
               <p style={{fontSize:'2.5rem'}} className="medium bold">  <i className="small material-icons">call</i> +971 4556 1906</p>
             </div>
          </div>
        </div>
        <div className="col m6 s12" data-aos="zoom-out-left" >
            <div className="row">
              <h4>For more information</h4>
              <div className="row">
                  <form className="col s12">
                    <div className="row">
                      <div className="input-field col s6">
                        <input  id="first_name" type="text" className="validate" name="firstName" value={data.firstName} onChange={handlChange} />
                        <label htmlFor="first_name">First Name</label>
                      </div>
                      <div className="input-field col s6">
                        <input id="last_name" type="text" className="validate" name="lastName" value={data.lastName} onChange={handlChange} />
                        <label htmlFor="last_name">Last Name</label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="input-field col s12">
                        <input  value="" id="phone" type="text" className="validate" name="phone"  value={data.phone} onChange={handlChange} /> 
                        <label htmlFor="phone">Phone</label>
                      </div>
                    </div>
                  
                    <div className="row">
                      <div className="input-field col s12">
                        <input id="email" type="email" className="validate" onChange={handlChange}  required/>
                        <label htmlFor="email">Email</label>
                      </div>
                    </div>
                    <button className="large btn waves-effect waves-light"  onClick={handleSubmit} name="action">Submit
                        <i className="material-icons right">send</i>
                      </button>
                  </form>
                </div>
              </div>
        </div>
      </div>
    </div>
  </section>
    )
}

export default ContactOne
