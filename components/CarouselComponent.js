import React from 'react'

const CarouselComponent = () => {
    return (
        <div id="carousel-section">
            <div className="carousel">
                <a className="carousel-item" href="#one!"><img src="./static/img/carousel-1.jpg" /></a>
                <a className="carousel-item" href="#two!"><img src="./static/img/carousel-2.jpg"/></a>
                <a className="carousel-item" href="#three!"><img src="./static/img/carousel-3.jpg" /></a>
                <a className="carousel-item" href="#four!"><img src="./static/img/carousel-4.jpg" /></a>
                <a className="carousel-item" href="#five!"><img src="./static/img/carousel-5.jpg" /></a>
             </div>
      </div>
    )
}

export default CarouselComponent
