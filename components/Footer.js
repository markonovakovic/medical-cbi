import React from 'react'

const Footer = () => {
    return (
        <footer className="bg-dark page-footer">
    <div className="container">
      <div className="row">
        
      </div>
      <div className="row">
        <div className="col  s6">
          <h5 className="white-text">Company Bio</h5>
          <p className="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


        </div>
        <div className="col s6 contacts-footer">
          <h5 className="white-text">Contacts</h5>
          <ul>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +971 4556 1906 UAE</a></li>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +603 2169 7057 Malaysia</a></li>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +381 11440 4362 Serbia</a></li>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +373 22999834 Moldova</a></li>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +66 99091 8357 also for WhatsAp</a></li>
            <li><a className="white-text" href="#!"><i className="tiny material-icons">call</i> +678 5355 7130 also for Viber</a></li>
          </ul>
      
               <ul><li><a className="text-primary" href="www.citizenship-program.com">www.citizenship-program.com</a></li>
               <li><a  className="text-primary" href="www.tcme.company">www.tcme.company</a></li>
               </ul>
           <p style={{fontSize: '1.1rem'}}>Uneingeschränkte Vollmacht

              internationales Firmenkonto mit Netbanking und Kreditkarte für die Offshore Firma</p>
        </div>
        
      </div>
    </div>
    <div className="footer-copyright">
      <div className="container" style={{fontSize:'12px'}}>
      Made by <a className="brown-text text-lighten-3" href="maxdesign.rs">Maxdesign</a>
      </div>
    </div>
 

  </footer>
    )
}

export default Footer
