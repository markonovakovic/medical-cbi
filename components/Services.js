import React from 'react'

const Services = () => {
    return (
    <section className="bg-dark valign-wrapper scrollspy" id="services">
        <div className="container">
           <div className="row">
                <div className="col m8 s12" data-aos="fade-right">
                    <div className="row">
                        <h2>Our Services</h2>
                        <div className="col m6 s12">
                            <ul>
                                <li>Concierge services</li>
                            
                                <li>Notary Public services</li>
                    
                                <li>Trust planning</li>

                                <li>Economic Relationship</li>
                                <li>Business Advisory Service</li>
                            </ul> 
                        </div>
                        <div className="col m6 s12">
                            <ul>
                                <li>Concierge services</li>
                            
                                <li>Notary Public services</li>
                    
                                <li>Trust planning</li>
                        
                                <li>Economic Relationship</li>
                                <li>Business Advisory Service</li>
                            </ul> 
                        </div>
                    </div>
                </div>
                <div className="col m4 s12" data-aos="fade-left">
                    <img className="materialboxed"  src="./static/img/images.jpg" />
                </div>
            </div>
        </div>
    </section>
    )
}

export default Services
