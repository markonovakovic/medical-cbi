import React from 'react'

export default function Hero() {
    return (
        <div id="index-banner" className="parallax-container">
        <div className="section no-pad-bot">
          <div className="banner-content container animated fadeIn">
            <br/><br/>
            <h1 className="header center teal-text text-lighten-2"> Global CBI </h1>
            <div className="row center">
              <p style={{color:'#333'}} className="header col s12 text-darken-4">GCI firm is a professional International Business Investment and Advisory Firm for Foreign Economic Relations.</p>
            </div>
            <div className="row center">
              <a href="#" id="download-button" className="btn-large waves-effect waves-light teal lighten-1">Get Started</a>
            </div>
            <br/><br/>
    
          </div>
        </div>
        <div className="parallax"><img src="./static/img/medical-marijuana.jpg" alt="Unsplashed background img 1" /></div>
      
    
       </div>
    )
}
