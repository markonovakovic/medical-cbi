import React from 'react'
import Link from 'next/link'

const links = [
  { href: 'https://zeit.co/now', label: 'ZEIT' },
  { href: 'https://github.com/zeit/next.js', label: 'GitHub' }
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`
  return link
})

const Nav = () => (

  <nav className="black" role="navigation">
    <div className="nav-wrapper container ">
       <a href="/#" className="navbar-brand">GCI<span className="nav_logo text-primary"> - Global CBI</span></a>
  
      <ul className="right hide-on-med-and-down">
   
        <li><Link href="/index">Home 1</Link></li>
        <li><Link href="/about">About</Link></li>
         <li><Link href="/contact">Contact</Link></li>
        <li><a href="#services">Services</a></li>
        <li><a href="#contact">Contact Us</a></li>
      </ul>

      <ul id="nav-mobile" className="sidenav">
        <li> <img src="./static/img/gci.jpg" style={{width:'200px'}}/></li>
          <li><a href="#about">About</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#contact">Contact Us</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
    </div>
 
{/*   <nav>
    <ul>
      <li>
        <Link href='/'>
          <a>Home</a>
        </Link>
      </li>
      {links.map(({ key, href, label }) => (
        <li key={key}>
          <a href={href}>{label}</a>
        </li>
      ))}
    </ul>
  */}
    
  </nav>
)

export default Nav
