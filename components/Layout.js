import React, { useEffect } from 'react'
import Head from 'next/head'
import Nav from './nav';
import Footer from './Footer'
import ContactOne from './ContactOne'

/* import 'materialize-css/dist/css/materialize.min.css'; 
import M from 'materialize-css/dist/js/materialize.min.js'; 
 */

export default function Layout(props) {


useEffect(() => {
    const targets = document.querySelectorAll('[data-src]');
    const lazyLoad =  target => {
    const io = new IntersectionObserver((entries, observer) =>{
     console.log(entries);
     entries.forEach(entry => {
       console.log('radi');

       if(entry.isIntersecting){
         const img = entry.target;
         const src = img.getAttribute('data-src');

          img.setAttribute('src', src);
          img.classList.add('fade');
          observer.disconnect();
       }
     })
   })
   io.observe(target)
 }

 targets.forEach(lazyLoad);
 M.AutoInit();
})
    return (
        <>
            <Head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Global CBI</title>

                <link rel='stylesheet' id='google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&#038;ver=1.0.0' type='text/css' media='all' />
                <link rel='stylesheet' id='font-awesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' type='text/css' media='all' />
                <script type='text/javascript' src='https://medicalcannabisservices.com.au/wp-includes/js/jquery/jquery.js'></script>
                <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
             
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
                <link href="./static/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
                <link href="./static/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
            </Head>
            <Nav />
            <div id="app">
                {props.children}
            <ContactOne />
            <Footer />
            <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
            <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
            <script src="./static/js/materialize.js"></script>
            <script  src="./static/js/init.js"></script>
            </div>


        </>
    )
}
