import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Layout from '../components/Layout';
import Hero from '../components/Hero';
import CarouselComponent from '../components/CarouselComponent';
import Services from '../components/Services';
import ContactOne from '../components/ContactOne';
import axios from 'axios'
import fetch from 'isomorphic-unfetch'

const Home = ({data, meta}) => {
 

   const content = data.content;
   return(
  <div>
    <Head>
      <title>{meta.title}</title>
      <link rel='icon' href='/favicon.ico' />
      <link rel='description' content={meta.description} />
       <meta name="keywords" content={meta.keywords}/>
       <meta name="robots" content={meta.robots} />
    @endif

    <meta name="page-topic" content="Design" />
    <meta name="author" content="Maxdesign" />
    <meta name="publisher" content="Maxdesign Belgrade Serbia" /> 
    </Head>

  <Layout>


    <div dangerouslySetInnerHTML={{__html: content}}></div>

  </Layout>
  </div>
   )
   }


   Home.getInitialProps = async ({ req }) => {
    const res = await fetch('http://medicalcbi.theconsultantmiddleeast.com/cannabis-macedonia')
    const json = await res.json()

    return { 
      data: json.data,
      meta: json.meta
     }
  }
  
export default Home
